 <?php
// set_include_path ( get_include_path () . PATH_SEPARATOR . '/src/Google' );
require_once dirname(__FILE__) . '/src/Google/autoload.php';

//Client Auth Settings
require_once 'client_set.php';
echo "<pre>";

/*
* Make any docs from tamplate
*/
class TemplateDocs
{
  public $fields;
  public $folderWithDocs = '0B4YuSf-4vxl2ZHZFakItREdIdWc';
  public $scriptId = 'MlotTbFEm99enVLqqg3ozMBLloF21-XnP';
  public $scriptFunction = 'myFunction';


  function __construct($templateId, $title, $clientFolderId)
  {
    $this->templateId = $templateId;
    $this->title = $title;
    $this->clientFolderId = $clientFolderId;
  }

  public function Fields($fields = array())
  {
    $this->fields = $fields;
  }

  public function MakeDocs()
  {
    // Get the API client and construct the service object.
    $service = new Google_Service_Script($client = getClient());

    //do replace with google script
    $request = new Google_Service_Script_ExecutionRequest(array(
      'function'   => $this->scriptFunction,
      'parameters' => array(
        'templateId'     => $this->templateId,
        'title'          => $this->title,
        'clientFolderId' => $this->clientFolderId,
        'fields'         => $this->fields,
        'folderWithDocs' => $this->folderWithDocs
        ),
      'devMode' => "true"
      ));

    return $service->scripts->run($this->scriptId, $request);
  }
}


//Data from $_POST
$clientName = '"ОАО Тяжпромсырвтор"';
$num = 25615;
$clientFolderId = "0025";


//Settings pemplate docs
// $folderWithDocumentsId = '0B4YuSf-4vxl2ZHZFakItREdIdWc';
$titleContract = "Договор №".$num."";
$titleInvoice  = "Справка-счет №".$num."";
$templateContractId = '1MtLhPaK6TwFjWJxIr_Kr13-cF5zsqD163WoisUjw4YA';
$templateInvoiceId = '1-o9b2aqNeSvqrSq0zRNyFIfF9d3rAzffKZkNKQ7JFn4';


$contract = New TemplateDocs($templateContractId, $titleContract, $clientFolderId);
$contract->Fields(['<CLIENT>' => $clientName,
                   '<NUM>'    => $num ]);
$request = $contract->MakeDocs();


